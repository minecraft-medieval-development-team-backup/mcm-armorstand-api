package com.mcm.armorstand.api.commands;

import com.mcm.armorstand.api.hashs.onGetId;
import com.mcm.armorstand.api.utils.AsSelected;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.Random;

public class Commands implements CommandExecutor {

    private static Random random;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        String uuid = player.getUniqueId().toString();

        if (command.getName().equalsIgnoreCase("armorstand") || command.getName().equalsIgnoreCase("as")) {
            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("create")) {
                    int id = new Random().nextInt(999999999);
                    ArmorStand stand = player.getWorld().spawn(player.getLocation(), ArmorStand.class);
                    stand.setCustomName(String.valueOf(id));
                    stand.setCustomNameVisible(false);

                    if (AsSelected.get(player.getUniqueId().toString()) == null)
                        new AsSelected(player.getUniqueId().toString(), id).insert();
                    else AsSelected.get(player.getUniqueId().toString()).setId(id);

                    player.sendMessage(ChatColor.GREEN + " * Armorstand criado, ID: " + ChatColor.UNDERLINE + id);
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else if (args[0].equalsIgnoreCase("move")) {
                    CommandSetPose.set(player, uuid, "move");
                } else if (args[0].equalsIgnoreCase("tphere")) {
                    CommandTp.tp(player, uuid);
                } else if (args[0].equalsIgnoreCase("tpback")) {
                    CommandTp.tpback(player, uuid);
                } else if (args[0].equalsIgnoreCase("getid")) {
                    if (onGetId.get(uuid) == null) {
                        player.sendMessage(ChatColor.GREEN + " * Modo seleção ativo!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        new onGetId(uuid).insert();
                    } else {
                        player.sendMessage(ChatColor.GREEN + " * Modo seleção desativado!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        onGetId.get(uuid).remove();
                    }
                }  else if (args[0].equalsIgnoreCase("clone")) {
                    if (AsSelected.get(uuid) != null) {
                        for (Entity entity : player.getNearbyEntities(30, 30, 30)) {
                            if (entity instanceof ArmorStand) {
                                if (entity != null && entity.getCustomName() != null && !entity.getCustomName().isEmpty() && entity.getCustomName().equalsIgnoreCase(String.valueOf(AsSelected.get(uuid).getId()))) {
                                    ArmorStand copy = player.getWorld().spawn(player.getLocation(), ArmorStand.class);
                                    copy.setItemInHand(((ArmorStand) entity).getItemInHand());
                                    copy.setLeggings(((ArmorStand) entity).getLeggings());
                                    copy.setHelmet(((ArmorStand) entity).getHelmet());
                                    copy.setChestplate(((ArmorStand) entity).getChestplate());
                                    copy.setBoots(((ArmorStand) entity).getBoots());
                                    copy.setLeftArmPose(((ArmorStand) entity).getLeftArmPose());
                                    copy.setRightArmPose(((ArmorStand) entity).getRightArmPose());
                                    copy.setLeftLegPose(((ArmorStand) entity).getLeftLegPose());
                                    copy.setRightLegPose(((ArmorStand) entity).getRightLegPose());
                                    copy.setHeadPose(((ArmorStand) entity).getHeadPose());
                                    copy.setBodyPose(((ArmorStand) entity).getBodyPose());
                                    copy.setRemoveWhenFarAway(((ArmorStand) entity).getRemoveWhenFarAway());
                                    copy.setInvulnerable(entity.isInvulnerable());
                                    copy.setGravity(entity.hasGravity());
                                    copy.setCollidable(((ArmorStand) entity).isCollidable());
                                    copy.setCanPickupItems(((ArmorStand) entity).getCanPickupItems());
                                    copy.setSmall(((ArmorStand) entity).isSmall());
                                    copy.setMarker(((ArmorStand) entity).isMarker());
                                    copy.setVisible(((ArmorStand) entity).isVisible());
                                    copy.setBasePlate(((ArmorStand) entity).hasBasePlate());
                                    copy.setArms(((ArmorStand) entity).hasArms());
                                    copy.setCustomNameVisible(false);
                                    int id = new Random().nextInt(999999999);
                                    copy.setCustomName(String.valueOf(id));

                                    if (AsSelected.get(player.getUniqueId().toString()) == null)
                                        new AsSelected(player.getUniqueId().toString(), id).insert();
                                    else AsSelected.get(player.getUniqueId().toString()).setId(id);

                                    player.sendMessage(ChatColor.GREEN + " * Armorstand replicado, ID: " + ChatColor.UNDERLINE + id);
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                } continue;
                            } continue;
                        }
                    } else {
                        player.sendMessage(ChatColor.GREEN + " * Ops, selecione uma entidade para clonar-la!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else if (args[0].equalsIgnoreCase("setallnamevisible")) {
                    for (Entity entity : player.getNearbyEntities(30, 30, 30)) {
                        if (entity instanceof ArmorStand) {
                            entity.setCustomNameVisible(true);
                        } continue;
                    }
                } else if (args[0].equalsIgnoreCase("idsnext")) {
                    String msg = null;
                    for (Entity entity : player.getNearbyEntities(30, 30, 30)) {
                        if (entity instanceof ArmorStand) {
                            if (!entity.getCustomName().isEmpty()) {
                                if (msg == null) {
                                    msg = entity.getCustomName();
                                } else msg += ", " + entity.getCustomName();
                            } continue;
                        } continue;
                    }

                    if (msg == null) {
                        player.sendMessage(ChatColor.RED + " * Ops, não existe nenhum armorstand editavel próximo!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    } else {
                        player.sendMessage(ChatColor.GREEN + " * Armorstands próximos: " + ChatColor.GRAY + msg);
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else if (args[0].equalsIgnoreCase("setbodypose")) {
                    CommandSetPose.set(player, uuid, "body");
                } else if (args[0].equalsIgnoreCase("setheadpose")) {
                    CommandSetPose.set(player, uuid, "head");
                } else if (args[0].equalsIgnoreCase("setleftarmpose")) {
                    CommandSetPose.set(player, uuid, "leftarm");
                } else if (args[0].equalsIgnoreCase("setrightarmpose")) {
                    CommandSetPose.set(player, uuid, "rightarm");
                } else if (args[0].equalsIgnoreCase("setleftlegpose")) {
                    CommandSetPose.set(player, uuid, "leftleg");
                } else if (args[0].equalsIgnoreCase("setrightlegpose")) {
                    CommandSetPose.set(player, uuid, "rightleg");
                } else if (args[0].equalsIgnoreCase("setboots")) {
                    CommandSetItem.set(player, uuid, "boots");
                } else if (args[0].equalsIgnoreCase("setchestplate")) {
                    CommandSetItem.set(player, uuid, "chestplate");
                } else if (args[0].equalsIgnoreCase("sethelmet")) {
                    CommandSetItem.set(player, uuid, "helmet");
                } else if (args[0].equalsIgnoreCase("setleggings")) {
                    CommandSetItem.set(player, uuid, "leggings");
                } else if (args[0].equalsIgnoreCase("setiteminhand")) {
                    CommandSetItem.set(player, uuid, "iteminhand");
                } else msgError(player);
            } else if (args.length == 2) {
                if (args[0].equalsIgnoreCase("kill")) {
                    try {
                        int radius = Integer.valueOf(args[1]);

                        for (Entity entity : player.getNearbyEntities(radius, radius, radius)) {
                            if (entity instanceof ArmorStand) {
                                entity.remove();
                            }
                        }
                    } catch (NumberFormatException e) {
                        player.sendMessage(ChatColor.RED + " * Ops, forneça um radius para que seja aplicado o kill as.");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    }
                } else if (args[0].equalsIgnoreCase("setid")) {
                    CommandSetId.set(player, uuid, args);
                } else if (args[0].equalsIgnoreCase("setarms")) {
                    CommandSetBooleanCase.set(player, uuid, args, "setarms");
                } else if (args[0].equalsIgnoreCase("setbaseplate")) {
                    CommandSetBooleanCase.set(player, uuid, args, "setbaseplate");
                } else if (args[0].equalsIgnoreCase("setmarker")) {
                    CommandSetBooleanCase.set(player, uuid, args, "setmarker");
                } else if (args[0].equalsIgnoreCase("setsmall")) {
                    CommandSetBooleanCase.set(player, uuid, args, "setsmall");
                } else if (args[0].equalsIgnoreCase("setvisible")) {
                    CommandSetBooleanCase.set(player, uuid, args, "setvisible");
                } else if (args[0].equalsIgnoreCase("setcanpickupitems")) {
                    CommandSetBooleanCase.set(player, uuid, args, "setcanpickupitems");
                } else if (args[0].equalsIgnoreCase("setcollidable")) {
                    CommandSetBooleanCase.set(player, uuid, args, "setcollidable");
                } else if (args[0].equalsIgnoreCase("setgravity")) {
                    CommandSetBooleanCase.set(player, uuid, args, "setgravity");
                } else if (args[0].equalsIgnoreCase("setinvulnerable")) {
                    CommandSetBooleanCase.set(player, uuid, args, "setinvulnerable");
                } else if (args[0].equalsIgnoreCase("setremovewhenfaraway")) {
                    CommandSetBooleanCase.set(player, uuid, args, "setremovewhenfaraway");
                } else msgError(player);
            } else if (args.length == 3) {
                if (args[0].equalsIgnoreCase("setbodypose")) {
                    CommandSetPose.set(player, uuid, args, "body");
                } else if (args[0].equalsIgnoreCase("setheadpose")) {
                    CommandSetPose.set(player, uuid, args, "head");
                } else if (args[0].equalsIgnoreCase("setleftarmpose")) {
                    CommandSetPose.set(player, uuid, args, "leftarm");
                } else if (args[0].equalsIgnoreCase("setrightarmpose")) {
                    CommandSetPose.set(player, uuid, args, "rightarm");
                } else if (args[0].equalsIgnoreCase("setleftlegpose")) {
                    CommandSetPose.set(player, uuid, args, "leftleg");
                } else if (args[0].equalsIgnoreCase("setrightlegpose")) {
                    CommandSetPose.set(player, uuid, args, "rightleg");
                } else if (args[0].equalsIgnoreCase("move")) {
                    CommandSetPose.set(player, uuid, args, "move");
                }
            }
        }
        return false;
    }

    private static void msgError(Player player) {
        player.sendMessage(ChatColor.RED + " \n * Comandos relacionados: \n" + ChatColor.GRAY + "\n  /as getid\n    /as setid (id)\n  /as create\n  /as move\n  /as tphere\n  /as tpback\n  /as clone\n  /as idsnext\n  /as setallnamevisible\n  /as setbodypose\n  /as setheadpose\n  /as setleftarmpose\n  /as setrightarmpose\n  /as setleftlegpose"
                + "\n  /as setrightpose\n  /as setboots\n  /as setchestplate\n  /as sethelmet\n  /as setleggings\n  /as setiteminhand\n  /as setarms (true/false)\n  /as setbaseplate (true/false)\n  /as setmarker (true/false)\n  /as setsmall (true/false)\n  /as setvisible (true/false)"
                + "\n  /as setcanpickupitems (true/ false)\n  /as setcollidable (true/false)\n  /as setgravity\n  /as setinvulnerable\n  /as setremovewhenfaraway\n  /as setvelocity\n  ");
        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
    }
}
