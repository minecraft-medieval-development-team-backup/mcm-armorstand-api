package com.mcm.armorstand.api.commands;

import com.mcm.armorstand.api.utils.AsSelected;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class CommandSetBooleanCase {

    public static void set(Player player, String uuid, String[] args, String subcm) {
        if (AsSelected.get(uuid) != null) {
            boolean set = true;
            if (args[1].equalsIgnoreCase("false") || args[1].equalsIgnoreCase("true")) {
                if (args[1].equalsIgnoreCase("false")) set = false;
                if (args[1].equalsIgnoreCase("true")) set = true;
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, você deve informar um valor (true ou false) para que prosseguirmos!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            }

            boolean has = false;
            for (Entity entity : player.getNearbyEntities(30, 30, 30)) {
                if (entity instanceof ArmorStand) {
                    if (entity != null && entity.getCustomName() != null && !entity.getCustomName().isEmpty() && entity.getCustomName().equalsIgnoreCase(String.valueOf(AsSelected.get(uuid).getId()))) {
                        has = true;
                        if (subcm.equalsIgnoreCase("setarms")) {
                            ((ArmorStand) entity).setArms(set);
                        } else if (subcm.equalsIgnoreCase("setbaseplate")) {
                            ((ArmorStand) entity).setBasePlate(set);
                        } else if (subcm.equalsIgnoreCase("setvisible")) {
                            ((ArmorStand) entity).setVisible(set);
                        } else if (subcm.equalsIgnoreCase("setmarker")) {
                            ((ArmorStand) entity).setMarker(set);
                        } else if (subcm.equalsIgnoreCase("setsmall")) {
                            ((ArmorStand) entity).setSmall(set);
                        } else if (subcm.equalsIgnoreCase("setcanpickupitems")) {
                            ((ArmorStand) entity).setCanPickupItems(set);
                        } else if (subcm.equalsIgnoreCase("setcollidable")) {
                            ((ArmorStand) entity).setCollidable(set);
                        } else if (subcm.equalsIgnoreCase("setgravity")) {
                            ((ArmorStand) entity).setGravity(set);
                        } else if (subcm.equalsIgnoreCase("setinvulnerable")) {
                            ((ArmorStand) entity).setInvulnerable(set);
                        } else if (subcm.equalsIgnoreCase("setremovewhenfaraway")) {
                            ((ArmorStand) entity).setRemoveWhenFarAway(set);
                        }
                    } continue;
                } continue;
            }

            if (has == false) {
                player.sendMessage(ChatColor.RED + " * Ops, não foi encontrado nenhuma Armorstand com o id definido dentro de um raio de 30 blocos!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            } else {
                player.sendMessage(ChatColor.GREEN + " * Armorstand atualizado com sucesso!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, você deve selecionar a um armorstand para editar-lo!");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        }
    }
}
