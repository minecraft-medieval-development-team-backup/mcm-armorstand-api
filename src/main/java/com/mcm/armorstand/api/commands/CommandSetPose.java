package com.mcm.armorstand.api.commands;

import com.mcm.armorstand.api.hashs.*;
import com.mcm.armorstand.api.utils.AsSelected;
import com.mcm.armorstand.api.utils.GiveItensEditor;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.EulerAngle;

import java.util.ArrayList;

public class CommandSetPose implements Listener {

    public static void set(Player player, String uuid, String pose) {
        if (AsSelected.get(uuid) != null) {
            if (onBodyPose.get(uuid) == null && onHeadPose.get(uuid) == null && onLeftArmPose.get(uuid) == null && onRightArmPose.get(uuid) == null && onLeftLegPose.get(uuid) == null && onRightLegPose.get(uuid) == null && onMovePose.get(uuid) == null) {
                if (pose.equalsIgnoreCase("body")) {
                    if (onBodyPose.get(uuid) == null) new onBodyPose(uuid).insert();
                } else if (pose.equalsIgnoreCase("head")) {
                    if (onHeadPose.get(uuid) == null) new onHeadPose(uuid).insert();
                } else if (pose.equalsIgnoreCase("leftarm")) {
                    if (onLeftArmPose.get(uuid) == null) new onLeftArmPose(uuid).insert();
                } else if (pose.equalsIgnoreCase("rightarm")) {
                    if (onRightArmPose.get(uuid) == null) new onRightArmPose(uuid).insert();
                } else if (pose.equalsIgnoreCase("leftleg")) {
                    if (onLeftLegPose.get(uuid) == null) new onLeftLegPose(uuid).insert();
                } else if (pose.equalsIgnoreCase("rightleg")) {
                    if (onRightLegPose.get(uuid) == null) new onRightLegPose(uuid).insert();
                } else if (pose.equalsIgnoreCase("move")) {
                    if (onMovePose.get(uuid) == null) new onMovePose(uuid).insert();
                }

                if (onMovePose.get(uuid) != null) {
                    GiveItensEditor.give(player, true);
                } else GiveItensEditor.give(player, false);
                player.sendMessage(ChatColor.GREEN + " * Edite a posição do " + pose + " utilizando os itens cedidos no inventário!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, você deve sair do atual modo de edição para fazer isto!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, você deve selecionar a um armorstand para editar-lo!");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        }
    }

    public static void set(Player player, String uuid, String[] args, String pose) {
        if (AsSelected.get(uuid) != null) {
            if (args[1].equalsIgnoreCase("x") || args[1].equalsIgnoreCase("y") || args[1].equalsIgnoreCase("z") || args[1].equalsIgnoreCase("yaw") || args[1].equalsIgnoreCase("pitch")) {
                if (onBodyPose.get(uuid) != null || onHeadPose.get(uuid) != null || onLeftArmPose.get(uuid) != null || onRightArmPose.get(uuid) != null || onLeftLegPose.get(uuid) != null || onRightLegPose.get(uuid) != null || onMovePose.get(uuid) != null) {
                    for (Entity entity : player.getNearbyEntities(30, 30, 30)) {
                        if (entity instanceof ArmorStand) {
                            if (entity != null && entity.getCustomName() != null && !entity.getCustomName().isEmpty() && entity.getCustomName().equalsIgnoreCase(String.valueOf(AsSelected.get(uuid).getId()))) {
                                ArmorStand armorStand = (ArmorStand) entity;

                                try {
                                    double value = Double.parseDouble(args[2]);

                                    if (pose.equalsIgnoreCase("body")) {
                                        if (args[1].equalsIgnoreCase("x")) {
                                            EulerAngle eulerAngle = new EulerAngle(value, armorStand.getBodyPose().getY(), armorStand.getBodyPose().getZ());
                                            armorStand.setBodyPose(eulerAngle);
                                        } else if (args[1].equalsIgnoreCase("y")) {
                                            EulerAngle eulerAngle = new EulerAngle(armorStand.getBodyPose().getX(), value, armorStand.getBodyPose().getZ());
                                            armorStand.setBodyPose(eulerAngle);
                                        } else if (args[1].equalsIgnoreCase("z")) {
                                            EulerAngle eulerAngle = new EulerAngle(armorStand.getBodyPose().getX(), armorStand.getBodyPose().getY(), value);
                                            armorStand.setBodyPose(eulerAngle);
                                        }
                                    } else if (pose.equalsIgnoreCase("head")) {
                                        if (args[1].equalsIgnoreCase("x")) {
                                            EulerAngle eulerAngle = new EulerAngle(value, armorStand.getHeadPose().getY(), armorStand.getHeadPose().getZ());
                                            armorStand.setBodyPose(eulerAngle);
                                        } else if (args[1].equalsIgnoreCase("y")) {
                                            EulerAngle eulerAngle = new EulerAngle(armorStand.getHeadPose().getX(), value, armorStand.getHeadPose().getZ());
                                            armorStand.setBodyPose(eulerAngle);
                                        } else if (args[1].equalsIgnoreCase("z")) {
                                            EulerAngle eulerAngle = new EulerAngle(armorStand.getHeadPose().getX(), armorStand.getHeadPose().getY(), value);
                                            armorStand.setBodyPose(eulerAngle);
                                        }
                                    } else if (pose.equalsIgnoreCase("leftarm")) {
                                        if (args[1].equalsIgnoreCase("x")) {
                                            EulerAngle eulerAngle = new EulerAngle(value, armorStand.getLeftArmPose().getY(), armorStand.getLeftArmPose().getZ());
                                            armorStand.setBodyPose(eulerAngle);
                                        } else if (args[1].equalsIgnoreCase("y")) {
                                            EulerAngle eulerAngle = new EulerAngle(armorStand.getLeftArmPose().getX(), value, armorStand.getLeftArmPose().getZ());
                                            armorStand.setBodyPose(eulerAngle);
                                        } else if (args[1].equalsIgnoreCase("z")) {
                                            EulerAngle eulerAngle = new EulerAngle(armorStand.getLeftArmPose().getX(), armorStand.getLeftArmPose().getY(), value);
                                            armorStand.setBodyPose(eulerAngle);
                                        }
                                    } else if (pose.equalsIgnoreCase("rightarm")) {
                                        if (args[1].equalsIgnoreCase("x")) {
                                            EulerAngle eulerAngle = new EulerAngle(value, armorStand.getRightArmPose().getY(), armorStand.getRightArmPose().getZ());
                                            armorStand.setBodyPose(eulerAngle);
                                        } else if (args[1].equalsIgnoreCase("y")) {
                                            EulerAngle eulerAngle = new EulerAngle(armorStand.getRightArmPose().getX(), value, armorStand.getRightArmPose().getZ());
                                            armorStand.setBodyPose(eulerAngle);
                                        } else if (args[1].equalsIgnoreCase("z")) {
                                            EulerAngle eulerAngle = new EulerAngle(armorStand.getRightArmPose().getX(), armorStand.getRightArmPose().getY(), value);
                                            armorStand.setBodyPose(eulerAngle);
                                        }
                                    } else if (pose.equalsIgnoreCase("leftleg")) {
                                        if (args[1].equalsIgnoreCase("x")) {
                                            EulerAngle eulerAngle = new EulerAngle(value, armorStand.getLeftArmPose().getY(), armorStand.getLeftArmPose().getZ());
                                            armorStand.setBodyPose(eulerAngle);
                                        } else if (args[1].equalsIgnoreCase("y")) {
                                            EulerAngle eulerAngle = new EulerAngle(armorStand.getLeftArmPose().getX(), value, armorStand.getLeftArmPose().getZ());
                                            armorStand.setBodyPose(eulerAngle);
                                        } else if (args[1].equalsIgnoreCase("z")) {
                                            EulerAngle eulerAngle = new EulerAngle(armorStand.getLeftArmPose().getX(), armorStand.getLeftArmPose().getY(), value);
                                            armorStand.setBodyPose(eulerAngle);
                                        }
                                    } else if (pose.equalsIgnoreCase("rightleg")) {
                                        if (args[1].equalsIgnoreCase("x")) {
                                            EulerAngle eulerAngle = new EulerAngle(value, armorStand.getRightArmPose().getY(), armorStand.getRightArmPose().getZ());
                                            armorStand.setBodyPose(eulerAngle);
                                        } else if (args[1].equalsIgnoreCase("y")) {
                                            EulerAngle eulerAngle = new EulerAngle(armorStand.getRightArmPose().getX(), value, armorStand.getRightArmPose().getZ());
                                            armorStand.setBodyPose(eulerAngle);
                                        } else if (args[1].equalsIgnoreCase("z")) {
                                            EulerAngle eulerAngle = new EulerAngle(armorStand.getRightArmPose().getX(), armorStand.getRightArmPose().getY(), value);
                                            armorStand.setBodyPose(eulerAngle);
                                        }
                                    } else if (pose.equalsIgnoreCase("move")) {
                                        if (args[1].equalsIgnoreCase("x")) {
                                            Location location = new Location(armorStand.getWorld(), value, armorStand.getLocation().getY(), armorStand.getLocation().getZ(), armorStand.getLocation().getYaw(), armorStand.getLocation().getPitch());
                                            armorStand.teleport(location);
                                        } else if (args[1].equalsIgnoreCase("y")) {
                                            Location location = new Location(armorStand.getWorld(), armorStand.getLocation().getX(), value, armorStand.getLocation().getZ(), armorStand.getLocation().getYaw(), armorStand.getLocation().getPitch());
                                            armorStand.teleport(location);
                                        } else if (args[1].equalsIgnoreCase("z")) {
                                            Location location = new Location(armorStand.getWorld(), armorStand.getLocation().getX(), armorStand.getLocation().getY(), value, armorStand.getLocation().getYaw(), armorStand.getLocation().getPitch());
                                            armorStand.teleport(location);
                                        } else if (args[1].equalsIgnoreCase("yaw")) {
                                            Float f = Float.parseFloat(args[2]);
                                            Location location = new Location(armorStand.getWorld(), armorStand.getLocation().getX(), armorStand.getLocation().getY(), armorStand.getLocation().getZ(), f, armorStand.getLocation().getPitch());
                                            armorStand.teleport(location);
                                        } else if (args[1].equalsIgnoreCase("pitch")) {
                                            Float f = Float.parseFloat(args[2]);
                                            Location location = new Location(armorStand.getWorld(), armorStand.getLocation().getX(), armorStand.getLocation().getY(), armorStand.getLocation().getZ(), armorStand.getLocation().getYaw(), f);
                                            armorStand.teleport(location);
                                        }
                                    }

                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                } catch (NumberFormatException e) {
                                    player.sendMessage(ChatColor.RED + " * Ops, alguma das informações contém erro!");
                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                                }
                            } continue;
                        } continue;
                    }
                }
            } else {
                player.sendMessage(ChatColor.RED + " * Ops, alguma das informações contém erro!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, você deve selecionar a um armorstand para editar-lo!");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        }
    }

    //---

    @EventHandler
    public void onMove(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        String uuid = player.getUniqueId().toString();

        if (onBodyPose.get(uuid) != null || onHeadPose.get(uuid) != null || onLeftArmPose.get(uuid) != null || onRightArmPose.get(uuid) != null || onLeftLegPose.get(uuid) != null || onRightLegPose.get(uuid) != null || onMovePose.get(uuid) != null) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        if (onBodyPose.get(uuid) != null || onHeadPose.get(uuid) != null || onLeftArmPose.get(uuid) != null || onRightArmPose.get(uuid) != null || onLeftLegPose.get(uuid) != null || onRightLegPose.get(uuid) != null || onMovePose.get(uuid) != null) {
            event.setCancelled(true);
            if (event.getPlayer().getItemInHand() != null && !event.getPlayer().getItemInHand().getType().equals(Material.AIR)) {
                if (event.getAction() != null) {
                    if (event.getAction().name().contains("RIGHT_CLICK") && player.getItemInHand().getType().equals(Material.ARMOR_STAND)) {
                        if (onBodyPose.get(uuid) != null) {
                            onBodyPose.get(uuid).remove();
                        } else if (onHeadPose.get(uuid) != null) {
                            onHeadPose.get(uuid).remove();
                        } else if (onRightLegPose.get(uuid) != null) {
                            onRightLegPose.get(uuid).remove();
                        } else if (onLeftLegPose.get(uuid) != null) {
                            onLeftLegPose.get(uuid).remove();
                        } else if (onRightArmPose.get(uuid) != null) {
                            onRightArmPose.get(uuid).remove();
                        } else if (onLeftArmPose.get(uuid) != null) {
                            onLeftArmPose.get(uuid).remove();
                        } else if (onMovePose.get(uuid) != null) {
                            onMovePose.get(uuid).remove();
                        }
                        player.getInventory().clear();
                        player.sendMessage(ChatColor.GREEN + " * Modo edição finalizado!");
                        player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        return;
                    }

                    //false = right = +
                    //true = left = -
                    boolean side = true;
                    if (event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK))
                        side = false;

                    if (onBodyPose.get(uuid) != null || onHeadPose.get(uuid) != null || onLeftArmPose.get(uuid) != null || onRightArmPose.get(uuid) != null || onLeftLegPose.get(uuid) != null || onRightLegPose.get(uuid) != null || onMovePose.get(uuid) != null) {
                        boolean has = false;
                        for (Entity entity : player.getNearbyEntities(30, 30, 30)) {
                            if (entity instanceof ArmorStand) {
                                if (entity != null && entity.getCustomName() != null && !entity.getCustomName().isEmpty() && entity.getCustomName().equalsIgnoreCase(String.valueOf(AsSelected.get(uuid).getId()))) {
                                    has = true;

                                    ArmorStand armorStand = (ArmorStand) entity;

                                    //X
                                    if (event.getPlayer().getItemInHand().getType().equals(Material.STICK)) {
                                        if (onMovePose.get(uuid) != null) {
                                            if (side == true) {
                                                Location location = new Location(armorStand.getWorld(), armorStand.getLocation().getX() - 0.1, armorStand.getLocation().getY(), armorStand.getLocation().getZ(), armorStand.getLocation().getYaw(), armorStand.getLocation().getPitch());
                                                armorStand.teleport(location);

                                                ArrayList<String> lore = new ArrayList<String>();
                                                lore.add(" ");
                                                lore.add(ChatColor.YELLOW + " X: " + ChatColor.GRAY + location.getX());
                                                lore.add(ChatColor.YELLOW + " Y: " + ChatColor.GRAY + location.getY());
                                                lore.add(ChatColor.YELLOW + " Z: " + ChatColor.GRAY + location.getZ());
                                                lore.add(ChatColor.YELLOW + " Yaw: " + ChatColor.GRAY + location.getYaw());
                                                lore.add(ChatColor.YELLOW + " Pitch: " + ChatColor.GRAY + location.getPitch());
                                                lore.add(" ");
                                                ItemStack iteminhand = player.getItemInHand().clone();
                                                ItemMeta metaItemInHand = iteminhand.getItemMeta();
                                                metaItemInHand.setLore(lore);
                                                player.getItemInHand().setItemMeta(metaItemInHand);
                                            } else {
                                                Location location = new Location(armorStand.getWorld(), armorStand.getLocation().getX() + 0.1, armorStand.getLocation().getY(), armorStand.getLocation().getZ(), armorStand.getLocation().getYaw(), armorStand.getLocation().getPitch());
                                                armorStand.teleport(location);

                                                ArrayList<String> lore = new ArrayList<String>();
                                                lore.add(" ");
                                                lore.add(ChatColor.YELLOW + " X: " + ChatColor.GRAY + location.getX());
                                                lore.add(ChatColor.YELLOW + " Y: " + ChatColor.GRAY + location.getY());
                                                lore.add(ChatColor.YELLOW + " Z: " + ChatColor.GRAY + location.getZ());
                                                lore.add(ChatColor.YELLOW + " Yaw: " + ChatColor.GRAY + location.getYaw());
                                                lore.add(ChatColor.YELLOW + " Pitch: " + ChatColor.GRAY + location.getPitch());
                                                lore.add(" ");
                                                ItemStack iteminhand = player.getItemInHand().clone();
                                                ItemMeta metaItemInHand = iteminhand.getItemMeta();
                                                metaItemInHand.setLore(lore);
                                                player.getItemInHand().setItemMeta(metaItemInHand);
                                            }
                                        } else if (onBodyPose.get(uuid) != null) {
                                            if (side == true) {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getBodyPose().getX() - 0.1, armorStand.getBodyPose().getY(), armorStand.getBodyPose().getZ());
                                                armorStand.setBodyPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            } else {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getBodyPose().getX() + 0.1, armorStand.getBodyPose().getY(), armorStand.getBodyPose().getZ());
                                                armorStand.setBodyPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            }
                                        } else if (onHeadPose.get(uuid) != null) {
                                            if (side == true) {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getHeadPose().getX() - 0.1, armorStand.getHeadPose().getY(), armorStand.getHeadPose().getZ());
                                                armorStand.setBodyPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            } else {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getHeadPose().getX() + 0.1, armorStand.getHeadPose().getY(), armorStand.getHeadPose().getZ());
                                                armorStand.setHeadPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            }
                                        } else if (onRightLegPose.get(uuid) != null) {
                                            if (side == true) {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getRightLegPose().getX() - 0.1, armorStand.getRightLegPose().getY(), armorStand.getRightLegPose().getZ());
                                                armorStand.setRightLegPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            } else {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getRightLegPose().getX() + 0.1, armorStand.getRightLegPose().getY(), armorStand.getRightLegPose().getZ());
                                                armorStand.setRightLegPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            }
                                        } else if (onLeftLegPose.get(uuid) != null) {
                                            if (side == true) {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getLeftLegPose().getX() - 0.1, armorStand.getLeftLegPose().getY(), armorStand.getLeftLegPose().getZ());
                                                armorStand.setLeftLegPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            } else {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getLeftLegPose().getX() + 0.1, armorStand.getLeftLegPose().getY(), armorStand.getLeftLegPose().getZ());
                                                armorStand.setLeftLegPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            }
                                        } else if (onRightArmPose.get(uuid) != null) {
                                            if (side == true) {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getRightArmPose().getX() - 0.1, armorStand.getRightArmPose().getY(), armorStand.getRightArmPose().getZ());
                                                armorStand.setRightArmPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            } else {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getRightArmPose().getX() + 0.1, armorStand.getRightArmPose().getY(), armorStand.getRightArmPose().getZ());
                                                armorStand.setRightArmPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            }
                                        } else if (onLeftArmPose.get(uuid) != null) {
                                            if (side == true) {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getLeftArmPose().getX() - 0.1, armorStand.getLeftArmPose().getY(), armorStand.getLeftArmPose().getZ());
                                                armorStand.setLeftArmPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            } else {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getLeftArmPose().getX() + 0.1, armorStand.getLeftArmPose().getY(), armorStand.getLeftArmPose().getZ());
                                                armorStand.setLeftArmPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            }
                                        }
                                    }

                                    //Y
                                    if (event.getPlayer().getItemInHand().getType().equals(Material.BONE)) {
                                        if (onMovePose.get(uuid) != null) {
                                            if (side == true) {
                                                Location location = new Location(armorStand.getWorld(), armorStand.getLocation().getX(), armorStand.getLocation().getY() - 0.1, armorStand.getLocation().getZ(), armorStand.getLocation().getYaw(), armorStand.getLocation().getPitch());
                                                armorStand.teleport(location);

                                                ArrayList<String> lore = new ArrayList<String>();
                                                lore.add(" ");
                                                lore.add(ChatColor.YELLOW + " X: " + ChatColor.GRAY + location.getX());
                                                lore.add(ChatColor.YELLOW + " Y: " + ChatColor.GRAY + location.getY());
                                                lore.add(ChatColor.YELLOW + " Z: " + ChatColor.GRAY + location.getZ());
                                                lore.add(ChatColor.YELLOW + " Yaw: " + ChatColor.GRAY + location.getYaw());
                                                lore.add(ChatColor.YELLOW + " Pitch: " + ChatColor.GRAY + location.getPitch());
                                                lore.add(" ");
                                                ItemStack iteminhand = player.getItemInHand().clone();
                                                ItemMeta metaItemInHand = iteminhand.getItemMeta();
                                                metaItemInHand.setLore(lore);
                                                player.getItemInHand().setItemMeta(metaItemInHand);
                                            } else {
                                                Location location = new Location(armorStand.getWorld(), armorStand.getLocation().getX(), armorStand.getLocation().getY() + 0.1, armorStand.getLocation().getZ(), armorStand.getLocation().getYaw(), armorStand.getLocation().getPitch());
                                                armorStand.teleport(location);

                                                ArrayList<String> lore = new ArrayList<String>();
                                                lore.add(" ");
                                                lore.add(ChatColor.YELLOW + " X: " + ChatColor.GRAY + location.getX());
                                                lore.add(ChatColor.YELLOW + " Y: " + ChatColor.GRAY + location.getY());
                                                lore.add(ChatColor.YELLOW + " Z: " + ChatColor.GRAY + location.getZ());
                                                lore.add(ChatColor.YELLOW + " Yaw: " + ChatColor.GRAY + location.getYaw());
                                                lore.add(ChatColor.YELLOW + " Pitch: " + ChatColor.GRAY + location.getPitch());
                                                lore.add(" ");
                                                ItemStack iteminhand = player.getItemInHand().clone();
                                                ItemMeta metaItemInHand = iteminhand.getItemMeta();
                                                metaItemInHand.setLore(lore);
                                                player.getItemInHand().setItemMeta(metaItemInHand);
                                            }
                                        } else if (onBodyPose.get(uuid) != null) {
                                            if (side == true) {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getBodyPose().getX(), armorStand.getBodyPose().getY() - 0.1, armorStand.getBodyPose().getZ());
                                                armorStand.setBodyPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            } else {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getBodyPose().getX(), armorStand.getBodyPose().getY() + 0.1, armorStand.getBodyPose().getZ());
                                                armorStand.setBodyPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            }
                                        } else if (onHeadPose.get(uuid) != null) {
                                            if (side == true) {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getHeadPose().getX(), armorStand.getHeadPose().getY() - 0.1, armorStand.getHeadPose().getZ());
                                                armorStand.setBodyPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            } else {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getHeadPose().getX(), armorStand.getHeadPose().getY() + 0.1, armorStand.getHeadPose().getZ());
                                                armorStand.setHeadPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            }
                                        } else if (onRightLegPose.get(uuid) != null) {
                                            if (side == true) {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getRightLegPose().getX(), armorStand.getRightLegPose().getY() - 0.1, armorStand.getRightLegPose().getZ());
                                                armorStand.setRightLegPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            } else {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getRightLegPose().getX(), armorStand.getRightLegPose().getY() + 0.1, armorStand.getRightLegPose().getZ());
                                                armorStand.setRightLegPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            }
                                        } else if (onLeftLegPose.get(uuid) != null) {
                                            if (side == true) {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getLeftLegPose().getX(), armorStand.getLeftLegPose().getY() - 0.1, armorStand.getLeftLegPose().getZ());
                                                armorStand.setLeftLegPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            } else {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getLeftLegPose().getX(), armorStand.getLeftLegPose().getY() + 0.1, armorStand.getLeftLegPose().getZ());
                                                armorStand.setLeftLegPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            }
                                        } else if (onRightArmPose.get(uuid) != null) {
                                            if (side == true) {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getRightArmPose().getX(), armorStand.getRightArmPose().getY() - 0.1, armorStand.getRightArmPose().getZ());
                                                armorStand.setRightArmPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            } else {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getRightArmPose().getX(), armorStand.getRightArmPose().getY() + 0.1, armorStand.getRightArmPose().getZ());
                                                armorStand.setRightArmPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            }
                                        } else if (onLeftArmPose.get(uuid) != null) {
                                            if (side == true) {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getLeftArmPose().getX(), armorStand.getLeftArmPose().getY() - 0.1, armorStand.getLeftArmPose().getZ());
                                                armorStand.setLeftArmPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            } else {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getLeftArmPose().getX(), armorStand.getLeftArmPose().getY() + 0.1, armorStand.getLeftArmPose().getZ());
                                                armorStand.setLeftArmPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            }
                                        }
                                    }

                                    //Z
                                    if (event.getPlayer().getItemInHand().getType().equals(Material.BLAZE_ROD)) {
                                        if (onMovePose.get(uuid) != null) {
                                            if (side == true) {
                                                Location location = new Location(armorStand.getWorld(), armorStand.getLocation().getX(), armorStand.getLocation().getY(), armorStand.getLocation().getZ() - 0.1, armorStand.getLocation().getYaw(), armorStand.getLocation().getPitch());
                                                armorStand.teleport(location);

                                                ArrayList<String> lore = new ArrayList<String>();
                                                lore.add(" ");
                                                lore.add(ChatColor.YELLOW + " X: " + ChatColor.GRAY + location.getX());
                                                lore.add(ChatColor.YELLOW + " Y: " + ChatColor.GRAY + location.getY());
                                                lore.add(ChatColor.YELLOW + " Z: " + ChatColor.GRAY + location.getZ());
                                                lore.add(ChatColor.YELLOW + " Yaw: " + ChatColor.GRAY + location.getYaw());
                                                lore.add(ChatColor.YELLOW + " Pitch: " + ChatColor.GRAY + location.getPitch());
                                                lore.add(" ");
                                                ItemStack iteminhand = player.getItemInHand().clone();
                                                ItemMeta metaItemInHand = iteminhand.getItemMeta();
                                                metaItemInHand.setLore(lore);
                                                player.getItemInHand().setItemMeta(metaItemInHand);
                                            } else {
                                                Location location = new Location(armorStand.getWorld(), armorStand.getLocation().getX(), armorStand.getLocation().getY(), armorStand.getLocation().getZ() + 0.1, armorStand.getLocation().getYaw(), armorStand.getLocation().getPitch());
                                                armorStand.teleport(location);

                                                ArrayList<String> lore = new ArrayList<String>();
                                                lore.add(" ");
                                                lore.add(ChatColor.YELLOW + " X: " + ChatColor.GRAY + location.getX());
                                                lore.add(ChatColor.YELLOW + " Y: " + ChatColor.GRAY + location.getY());
                                                lore.add(ChatColor.YELLOW + " Z: " + ChatColor.GRAY + location.getZ());
                                                lore.add(ChatColor.YELLOW + " Yaw: " + ChatColor.GRAY + location.getYaw());
                                                lore.add(ChatColor.YELLOW + " Pitch: " + ChatColor.GRAY + location.getPitch());
                                                lore.add(" ");
                                                ItemStack iteminhand = player.getItemInHand().clone();
                                                ItemMeta metaItemInHand = iteminhand.getItemMeta();
                                                metaItemInHand.setLore(lore);
                                                player.getItemInHand().setItemMeta(metaItemInHand);
                                            }
                                        } else if (onBodyPose.get(uuid) != null) {
                                            if (side == true) {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getBodyPose().getX(), armorStand.getBodyPose().getY(), armorStand.getBodyPose().getZ() - 0.1);
                                                armorStand.setBodyPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            } else {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getBodyPose().getX(), armorStand.getBodyPose().getY(), armorStand.getBodyPose().getZ() + 0.1);
                                                armorStand.setBodyPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            }
                                        } else if (onHeadPose.get(uuid) != null) {
                                            if (side == true) {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getHeadPose().getX(), armorStand.getHeadPose().getY(), armorStand.getHeadPose().getZ() - 0.1);
                                                armorStand.setBodyPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            } else {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getHeadPose().getX(), armorStand.getHeadPose().getY(), armorStand.getHeadPose().getZ() + 0.1);
                                                armorStand.setHeadPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            }
                                        } else if (onRightLegPose.get(uuid) != null) {
                                            if (side == true) {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getRightLegPose().getX(), armorStand.getRightLegPose().getY(), armorStand.getRightLegPose().getZ() - 0.1);
                                                armorStand.setRightLegPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            } else {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getRightLegPose().getX(), armorStand.getRightLegPose().getY(), armorStand.getRightLegPose().getZ() + 0.1);
                                                armorStand.setRightLegPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            }
                                        } else if (onLeftLegPose.get(uuid) != null) {
                                            if (side == true) {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getLeftLegPose().getX(), armorStand.getLeftLegPose().getY(), armorStand.getLeftLegPose().getZ() - 0.1);
                                                armorStand.setLeftLegPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            } else {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getLeftLegPose().getX(), armorStand.getLeftLegPose().getY(), armorStand.getLeftLegPose().getZ() + 0.1);
                                                armorStand.setLeftLegPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            }
                                        } else if (onRightArmPose.get(uuid) != null) {
                                            if (side == true) {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getRightArmPose().getX(), armorStand.getRightArmPose().getY(), armorStand.getRightArmPose().getZ() - 0.1);
                                                armorStand.setRightArmPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            } else {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getRightArmPose().getX(), armorStand.getRightArmPose().getY(), armorStand.getRightArmPose().getZ() + 0.1);
                                                armorStand.setRightArmPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            }
                                        } else if (onLeftArmPose.get(uuid) != null) {
                                            if (side == true) {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getLeftArmPose().getX(), armorStand.getLeftArmPose().getY(), armorStand.getLeftArmPose().getZ() - 0.1);
                                                armorStand.setLeftArmPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            } else {
                                                EulerAngle eulerAngle = new EulerAngle(armorStand.getLeftArmPose().getX(), armorStand.getLeftArmPose().getY(), armorStand.getLeftArmPose().getZ() + 0.1);
                                                armorStand.setLeftArmPose(eulerAngle);
                                                updateItemInfo(player, eulerAngle);
                                            }
                                        }
                                    }

                                    //YAW
                                    if (event.getPlayer().getItemInHand().getType().equals(Material.FEATHER)) {
                                        if (onMovePose.get(uuid) != null) {
                                            if (side == true) {
                                                Location location = new Location(armorStand.getWorld(), armorStand.getLocation().getX(), armorStand.getLocation().getY(), armorStand.getLocation().getZ(), armorStand.getLocation().getYaw() - (float) 0.1, armorStand.getLocation().getPitch());
                                                armorStand.teleport(location);
                                            } else {
                                                Location location = new Location(armorStand.getWorld(), armorStand.getLocation().getX(), armorStand.getLocation().getY(), armorStand.getLocation().getZ(), armorStand.getLocation().getYaw() + (float) 0.1, armorStand.getLocation().getPitch());
                                                armorStand.teleport(location);
                                            }
                                        }
                                    }

                                    //PITCH
                                    if (event.getPlayer().getItemInHand().getType().equals(Material.LEGACY_LEASH)) {
                                        if (onMovePose.get(uuid) != null) {
                                            if (side == true) {
                                                Location location = new Location(armorStand.getWorld(), armorStand.getLocation().getX(), armorStand.getLocation().getY(), armorStand.getLocation().getZ(), armorStand.getLocation().getYaw(), armorStand.getLocation().getPitch() - (float) 0.1);
                                                armorStand.teleport(location);

                                                ArrayList<String> lore = new ArrayList<String>();
                                                lore.add(" ");
                                                lore.add(ChatColor.YELLOW + " X: " + ChatColor.GRAY + location.getX());
                                                lore.add(ChatColor.YELLOW + " Y: " + ChatColor.GRAY + location.getY());
                                                lore.add(ChatColor.YELLOW + " Z: " + ChatColor.GRAY + location.getZ());
                                                lore.add(ChatColor.YELLOW + " Yaw: " + ChatColor.GRAY + location.getYaw());
                                                lore.add(ChatColor.YELLOW + " Pitch: " + ChatColor.GRAY + location.getPitch());
                                                lore.add(" ");
                                                ItemStack iteminhand = player.getItemInHand().clone();
                                                ItemMeta metaItemInHand = iteminhand.getItemMeta();
                                                metaItemInHand.setLore(lore);
                                                player.getItemInHand().setItemMeta(metaItemInHand);
                                            } else {
                                                Location location = new Location(armorStand.getWorld(), armorStand.getLocation().getX(), armorStand.getLocation().getY(), armorStand.getLocation().getZ(), armorStand.getLocation().getYaw(), armorStand.getLocation().getPitch() + (float) 0.1);
                                                armorStand.teleport(location);

                                                ArrayList<String> lore = new ArrayList<String>();
                                                lore.add(" ");
                                                lore.add(ChatColor.YELLOW + " X: " + ChatColor.GRAY + location.getX());
                                                lore.add(ChatColor.YELLOW + " Y: " + ChatColor.GRAY + location.getY());
                                                lore.add(ChatColor.YELLOW + " Z: " + ChatColor.GRAY + location.getZ());
                                                lore.add(ChatColor.YELLOW + " Yaw: " + ChatColor.GRAY + location.getYaw());
                                                lore.add(ChatColor.YELLOW + " Pitch: " + ChatColor.GRAY + location.getPitch());
                                                lore.add(" ");
                                                ItemStack iteminhand = player.getItemInHand().clone();
                                                ItemMeta metaItemInHand = iteminhand.getItemMeta();
                                                metaItemInHand.setLore(lore);
                                                player.getItemInHand().setItemMeta(metaItemInHand);
                                            }
                                        }
                                    }

                                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                                } continue;
                            } continue;
                        }

                        if (has == false) {
                            player.sendMessage(ChatColor.RED + " * Ops, não foi encontrado nenhuma Armorstand com o id definido dentro de um raio de 30 blocos!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        } else {
                            player.sendMessage(ChatColor.GREEN + " * Armorstand atualizado com sucesso!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                        }
                    }
                }
            }
        }
    }

    private static void updateItemInfo(Player player, EulerAngle eulerAngle) {
        ArrayList<String> lore = new ArrayList<String>();
        lore.add(" ");
        lore.add(ChatColor.YELLOW + " X: " + ChatColor.GRAY + eulerAngle.getX());
        lore.add(ChatColor.YELLOW + " Y: " + ChatColor.GRAY + eulerAngle.getY());
        lore.add(ChatColor.YELLOW + " Z: " + ChatColor.GRAY + eulerAngle.getZ());
        lore.add(" ");
        ItemStack iteminhand = player.getItemInHand().clone();
        ItemMeta metaItemInHand = iteminhand.getItemMeta();
        metaItemInHand.setLore(lore);
        player.getItemInHand().setItemMeta(metaItemInHand);
    }
}
