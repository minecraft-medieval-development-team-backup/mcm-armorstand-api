package com.mcm.armorstand.api.commands;

import com.mcm.armorstand.api.utils.AsSelected;
import com.mcm.armorstand.api.utils.LastStandPos;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class CommandTp {

    public static void tp(Player player, String uuid) {
        if (AsSelected.get(uuid) != null) {
            boolean has = false;
            for (Entity entity : player.getNearbyEntities(30, 30, 30)) {
                if (entity instanceof ArmorStand) {
                    if (entity != null && entity.getCustomName() != null && !entity.getCustomName().isEmpty() && entity.getCustomName().equalsIgnoreCase(String.valueOf(AsSelected.get(uuid).getId()))) {
                        has = true;
                        if (LastStandPos.get(Integer.valueOf(entity.getCustomName())) == null) new LastStandPos(Integer.valueOf(entity.getCustomName()), entity.getLocation()).insert(); else LastStandPos.get(Integer.valueOf(entity.getCustomName())).setLocation(entity.getLocation());
                        entity.teleport(player.getLocation());
                    }
                }
            }

            if (has == false) {
                player.sendMessage(ChatColor.RED + " * Ops, não foi encontrado nenhuma Armorstand com o id definido dentro de um raio de 30 blocos!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            } else {
                player.sendMessage(ChatColor.GREEN + " * Armorstand teletransportado com sucesso!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, você deve selecionar a um armorstand para teletransportar-lo!");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        }
    }

    public static void tpback(Player player, String uuid) {
        if (AsSelected.get(uuid) != null) {
            boolean has = false;
            for (Entity entity : player.getNearbyEntities(30, 30, 30)) {
                if (entity instanceof ArmorStand) {
                    if (entity != null && entity.getCustomName() != null && !entity.getCustomName().isEmpty() && entity.getCustomName().equalsIgnoreCase(String.valueOf(AsSelected.get(uuid).getId()))) {
                        has = true;

                        if (LastStandPos.get(Integer.valueOf(entity.getCustomName())) != null) {
                            entity.teleport(LastStandPos.get(Integer.valueOf(entity.getCustomName())).getLocation());
                            if (LastStandPos.get(Integer.valueOf(entity.getCustomName())) == null) new LastStandPos(Integer.valueOf(entity.getCustomName()), entity.getLocation()).insert(); else LastStandPos.get(Integer.valueOf(entity.getCustomName())).setLocation(entity.getLocation());
                        } else {
                            player.sendMessage(ChatColor.RED + " * Ops, está entidade não tem uma coordenada anterior para retornar!");
                            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                        }
                    } continue;
                } continue;
            }

            if (has == false) {
                player.sendMessage(ChatColor.RED + " * Ops, não foi encontrado nenhuma Armorstand com o id definido dentro de um raio de 30 blocos!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            } else {
                player.sendMessage(ChatColor.GREEN + " * Armorstand teletransportado com sucesso!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, você deve selecionar a um armorstand para teletransportar-lo!");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        }
    }
}
