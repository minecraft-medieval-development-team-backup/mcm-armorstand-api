package com.mcm.armorstand.api.commands;

import com.mcm.armorstand.api.utils.AsSelected;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CommandSetItem {

    public static void set(Player player, String uuid, String local) {
        if (AsSelected.get(uuid) != null) {
            ItemStack inHand = player.getItemInHand().clone();

            if (inHand == null) {
                player.sendMessage(ChatColor.RED + " * Ops, você deve estar a segurar um item para definir-lo ao " + local + "!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                return;
            }

            boolean has = false;
            for (Entity entity : player.getNearbyEntities(30, 30, 30)) {
                if (entity instanceof ArmorStand) {
                    if (entity != null && entity.getCustomName() != null && !entity.getCustomName().isEmpty() && entity.getCustomName().equalsIgnoreCase(String.valueOf(AsSelected.get(uuid).getId()))) {
                        has = true;
                        if (local.equalsIgnoreCase("boots")) {
                            ((ArmorStand) entity).setBoots(inHand);
                        } else if (local.equalsIgnoreCase("chestplate")) {
                            ((ArmorStand) entity).setChestplate(inHand);
                        } else if (local.equalsIgnoreCase("helmet")) {
                            ((ArmorStand) entity).setHelmet(inHand);
                        } else if (local.equalsIgnoreCase("leggings")) {
                            ((ArmorStand) entity).setLeggings(inHand);
                        } else if (local.equalsIgnoreCase("iteminhand")) {
                            ((ArmorStand) entity).setItemInHand(inHand);
                        }
                    } continue;
                } continue;
            }

            if (has == false) {
                player.sendMessage(ChatColor.RED + " * Ops, não foi encontrado nenhuma Armorstand com o id definido dentro de um raio de 30 blocos!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            } else {
                player.sendMessage(ChatColor.GREEN + " * Armorstand atualizado com sucesso!");
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        } else {
            player.sendMessage(ChatColor.RED + " * Ops, você deve selecionar a um armorstand para editar-lo!");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        }
    }
}
