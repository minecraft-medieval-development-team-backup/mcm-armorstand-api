package com.mcm.armorstand.api.commands;

import com.mcm.armorstand.api.utils.AsSelected;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class CommandSetId {

    public static void set(Player player, String uuid, String[] args) {
        try {
            int id = Integer.valueOf(args[1]);
            if (AsSelected.get(uuid) == null) new AsSelected(uuid, id).insert(); else AsSelected.get(uuid).setId(id);

            player.sendMessage(ChatColor.GREEN + " * ID setado para " + ChatColor.UNDERLINE + id);
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        } catch (NumberFormatException e) {
            player.sendMessage(ChatColor.RED + " * Ops, o id fornecido contém algum erro!");
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
        }
    }
}
