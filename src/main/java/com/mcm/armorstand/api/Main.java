package com.mcm.armorstand.api;

import com.mcm.armorstand.api.commands.*;
import com.mcm.armorstand.api.listeners.PlayerSelectArmorStand;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    // -/as or /armorstand
    // -/as setid (id)
    // -/as (create/criar)
    // -/as setbodypose
    // -/as setheadpose
    // -/as setleftarmpose
    // -/as setrightarmpose
    // -/as setleftlegpose
    // -/as setrightlegpose
    // -/as setboots
    // -/as setchestplate
    // -/as sethelmet
    // -/as setleggings
    // -/as setiteminhand
    // -/as setarms (false/true)
    // -/as setbaseplate (false/true)
    // -/as setmarker (false/true)
    // -/as setsmall (false/true)
    // -/as setvisible (false/true)
    // -/as setcanpickupitems (false/true)
    // -/as setcollidable (false/true)
    // -/as setgravity (false/true)
    // -/as setinvulnerable (false/true)
    // -/as setremovewhenfaraway (false/true)
    // -/as setvelocity
    // -/as setallnamevisible
    // -/as getid
    // -/as move
    // -/as clone
    // -/as tphere
    // -/as tpback
    // -/as idsnext

    public static Main instance;
    public static Plugin plugin;

    public static Main getInstance() {
        return Main.instance;
    }

    @Override
    public void onEnable() {
        Main.instance = this;
        Main.plugin = this;
        Main.instance.getCommand("armorstand").setExecutor(new Commands());
        Main.instance.getCommand("as").setExecutor(new Commands());
        Bukkit.getPluginManager().registerEvents(new PlayerSelectArmorStand(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new CommandSetPose(), Main.plugin);
    }

    @Override
    public void onDisable() {
        //-
    }
}
