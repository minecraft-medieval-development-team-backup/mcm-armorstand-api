package com.mcm.armorstand.api.hashs;

import java.util.HashMap;

public class onGetId {

    private String uuid;
    private static HashMap<String, onGetId> cache = new HashMap<String, onGetId>();

    public onGetId(String uuid) {
        this.uuid = uuid;
    }

    public onGetId insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static onGetId get(String uuid) {
        return cache.get(uuid);
    }

    public void remove() {
        cache.remove(this.uuid);
    }
}
