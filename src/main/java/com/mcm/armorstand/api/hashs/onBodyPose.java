package com.mcm.armorstand.api.hashs;

import java.util.HashMap;

public class onBodyPose {

    private String uuid;
    private static HashMap<String, onBodyPose> cache = new HashMap<String, onBodyPose>();

    public onBodyPose(String uuid) {
        this.uuid = uuid;
    }

    public onBodyPose insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static onBodyPose get(String uuid) {
        return cache.get(uuid);
    }

    public void remove() {
        cache.remove(this.uuid);
    }
}
