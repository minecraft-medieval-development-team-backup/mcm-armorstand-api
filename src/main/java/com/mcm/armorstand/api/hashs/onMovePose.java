package com.mcm.armorstand.api.hashs;

import java.util.HashMap;

public class onMovePose {

    private String uuid;
    private static HashMap<String, onMovePose> cache = new HashMap<String, onMovePose>();

    public onMovePose(String uuid) {
        this.uuid = uuid;
    }

    public onMovePose insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static onMovePose get(String uuid) {
        return cache.get(uuid);
    }

    public void remove() {
        cache.remove(this.uuid);
    }
}
