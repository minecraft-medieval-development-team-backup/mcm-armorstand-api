package com.mcm.armorstand.api.hashs;

import java.util.HashMap;

public class onLeftLegPose {

    private String uuid;
    private static HashMap<String, onLeftLegPose> cache = new HashMap<String, onLeftLegPose>();

    public onLeftLegPose(String uuid) {
        this.uuid = uuid;
    }

    public onLeftLegPose insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static onLeftLegPose get(String uuid) {
        return cache.get(uuid);
    }

    public void remove() {
        cache.remove(this.uuid);
    }
}
