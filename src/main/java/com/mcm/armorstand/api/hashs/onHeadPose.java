package com.mcm.armorstand.api.hashs;

import java.util.HashMap;

public class onHeadPose {

    private String uuid;
    private static HashMap<String, onHeadPose> cache = new HashMap<String, onHeadPose>();

    public onHeadPose(String uuid) {
        this.uuid = uuid;
    }

    public onHeadPose insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static onHeadPose get(String uuid) {
        return cache.get(uuid);
    }

    public void remove() {
        cache.remove(this.uuid);
    }
}
