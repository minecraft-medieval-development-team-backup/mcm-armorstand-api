package com.mcm.armorstand.api.hashs;

import java.util.HashMap;

public class onLeftArmPose {

    private String uuid;
    private static HashMap<String, onLeftArmPose> cache = new HashMap<String, onLeftArmPose>();

    public onLeftArmPose(String uuid) {
        this.uuid = uuid;
    }

    public onLeftArmPose insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static onLeftArmPose get(String uuid) {
        return cache.get(uuid);
    }

    public void remove() {
        cache.remove(this.uuid);
    }
}
