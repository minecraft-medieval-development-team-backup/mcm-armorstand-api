package com.mcm.armorstand.api.hashs;

import java.util.HashMap;

public class onRightArmPose {

    private String uuid;
    private static HashMap<String, onRightArmPose> cache = new HashMap<String, onRightArmPose>();

    public onRightArmPose(String uuid) {
        this.uuid = uuid;
    }

    public onRightArmPose insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static onRightArmPose get(String uuid) {
        return cache.get(uuid);
    }

    public void remove() {
        cache.remove(this.uuid);
    }
}
