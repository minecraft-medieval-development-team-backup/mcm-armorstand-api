package com.mcm.armorstand.api.hashs;

import java.util.HashMap;

public class onRightLegPose {

    private String uuid;
    private static HashMap<String, onRightLegPose> cache = new HashMap<String, onRightLegPose>();

    public onRightLegPose(String uuid) {
        this.uuid = uuid;
    }

    public onRightLegPose insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static onRightLegPose get(String uuid) {
        return cache.get(uuid);
    }

    public void remove() {
        cache.remove(this.uuid);
    }
}
