package com.mcm.armorstand.api.listeners;

import com.mcm.armorstand.api.hashs.onGetId;
import com.mcm.armorstand.api.utils.AsSelected;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;

public class PlayerSelectArmorStand implements Listener {

    @EventHandler
    public void onClick(PlayerInteractAtEntityEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        if (onGetId.get(uuid) != null) {
            if (event.getRightClicked() instanceof ArmorStand) {
                ArmorStand armorStand = (ArmorStand) event.getRightClicked();

                if (!armorStand.getCustomName().isEmpty()) {
                    player.sendMessage(ChatColor.GREEN + " * ID: " + ChatColor.UNDERLINE + armorStand.getCustomName());
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    if (AsSelected.get(uuid) == null)
                        new AsSelected(uuid, Integer.valueOf(armorStand.getCustomName())).insert();
                    else AsSelected.get(uuid).setId(Integer.valueOf(armorStand.getCustomName()));
                }
            }
        }
    }
}
