package com.mcm.armorstand.api.utils;

import java.util.HashMap;

public class AsSelected {

    private String uuid;
    private int id;
    private static HashMap<String, AsSelected> cache = new HashMap<String, AsSelected>();

    public AsSelected (String uuid, int id) {
        this.uuid = uuid;
        this.id = id;
    }

    public AsSelected insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static AsSelected get(String uuid) {
        return cache.get(uuid);
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
