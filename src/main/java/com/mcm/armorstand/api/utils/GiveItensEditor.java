package com.mcm.armorstand.api.utils;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class GiveItensEditor {

    public static void give(Player player, boolean yawpitch) {
        ItemStack stickX = new ItemStack(Material.STICK);
        ItemMeta metaX = stickX.getItemMeta();
        metaX.setDisplayName(ChatColor.YELLOW + "X");
        metaX.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaX.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaX.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        metaX.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaX.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaX.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        stickX.setItemMeta(metaX);

        ItemStack arrowY = new ItemStack(Material.BONE);
        ItemMeta metaY = arrowY.getItemMeta();
        metaY.setDisplayName(ChatColor.YELLOW + "Y");
        metaY.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaY.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaY.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        metaY.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaY.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaY.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        arrowY.setItemMeta(metaY);

        ItemStack blazeZ = new ItemStack(Material.BLAZE_ROD);
        ItemMeta metaZ = blazeZ.getItemMeta();
        metaZ.setDisplayName(ChatColor.YELLOW + "Z");
        metaZ.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaZ.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaZ.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        metaZ.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaZ.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaZ.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        blazeZ.setItemMeta(metaZ);

        ItemStack yaw = new ItemStack(Material.FEATHER);
        ItemMeta metaYaw = yaw.getItemMeta();
        metaYaw.setDisplayName(ChatColor.YELLOW + "YAW");
        metaYaw.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaYaw.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaYaw.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        metaYaw.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaYaw.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaYaw.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        yaw.setItemMeta(metaYaw);

        ItemStack pitch = new ItemStack(Material.LEGACY_LEASH);
        ItemMeta metaPitch = pitch.getItemMeta();
        metaPitch.setDisplayName(ChatColor.YELLOW + "PITCH");
        metaPitch.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        metaPitch.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        metaPitch.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        metaPitch.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        metaPitch.addItemFlags(ItemFlag.HIDE_DESTROYS);
        metaPitch.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        pitch.setItemMeta(metaPitch);

        ItemStack close = new ItemStack(Material.ARMOR_STAND);
        ItemMeta metaClose = close.getItemMeta();
        metaClose.setDisplayName(ChatColor.RED + "Finalizar!");
        close.setItemMeta(metaClose);

        player.getInventory().clear();
        player.getInventory().setItem(3, stickX);
        player.getInventory().setItem(4, arrowY);
        player.getInventory().setItem(5, blazeZ);
        if (yawpitch == true) {
            player.getInventory().setItem(2, yaw);
            player.getInventory().setItem(6, pitch);
        }
        player.getInventory().setItem(8, close);
    }
}
