package com.mcm.armorstand.api.utils;

import org.bukkit.Location;

import java.util.HashMap;

public class LastStandPos {

    private int id;
    private Location location;
    private static HashMap<Integer, LastStandPos> cache = new HashMap<Integer, LastStandPos>();

    public LastStandPos(int id, Location location) {
        this.id = id;
        this.location = location;
    }

    public LastStandPos insert() {
        this.cache.put(this.id, this);
        return this;
    }

    public static LastStandPos get(int id) {
        return cache.get(id);
    }

    public Location getLocation() {
        return this.location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void remove() {
        this.cache.remove(this.id);
    }
}
